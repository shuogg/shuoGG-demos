package com.shuo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Created by shuoGG on 2018/10/27 0027
 */
@Mojo(name = "shuoGG", defaultPhase = LifecyclePhase.PACKAGE)
public class ShuoPlugin extends AbstractMojo {

    @Parameter
    private String msg;

    public void execute() {
        System.out.println("Msg is " + msg);
        System.out.println("Ya shuoGG");
    }
}
