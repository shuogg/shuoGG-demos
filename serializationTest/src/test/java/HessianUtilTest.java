import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HessianUtilTest {

    private Hessian2Util hessian2Util;

    private JsonRecBean bean;

    @Before
    public void init() {
        hessian2Util = Hessian2Util.getInstance();
        bean = new JsonRecBean();
        bean.setApp(1);
        bean.setMac(123456);
        bean.setName("TestBean");
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        bean.setList(list);
        Map<String,Integer> map = new HashMap<>();
        map.put("Tom", 12);
        map.put("Jenny", 13);
        map.put("Mic", 22);
        bean.setMap(map);
        bean.setValue(new byte[]{8, 8, 8, 8, 8});
    }


    @Test
    public void serializeJDK() {
        byte[] dataH = JdkSerializeUtil.serialize(bean);
        JsonRecBean beanH = (JsonRecBean) JdkSerializeUtil.deserialize(dataH);
        System.out.println(beanH);
        System.out.println(dataH.length);
    }

    @Test
    public void serializeH1() {
        byte[] dataH = HessianUtil.serialize(bean);
        JsonRecBean beanH = (JsonRecBean) HessianUtil.deserialize(dataH);
        System.out.println(beanH);
        System.out.println(dataH.length);
    }


    @Test
    public void serializeH2() {
        byte[] dataH = hessian2Util.serialize(bean);
        JsonRecBean beanH = (JsonRecBean) hessian2Util.deserialize(dataH);
        System.out.println(beanH);
        System.out.println(dataH.length);
    }

}