import java.io.*;

/**
 * Created by shuoGG on 2018/10/18
 */
public class JdkSerializeUtil {
    public static byte[] serialize(Object obj) {
        if (obj == null) throw new NullPointerException();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(os);
            out.writeObject(obj);
        } catch (IOException e) {
            throw new RuntimeException("IO错误", e);
        }
        return os.toByteArray();
    }

    public static Object deserialize(byte[] by) {
        if (by == null) throw new NullPointerException();
        ByteArrayInputStream is = new ByteArrayInputStream(by);
        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(is);
            return in.readObject();
        } catch (Exception e) {
            throw new RuntimeException("IO错误", e);
        }
    }
}
