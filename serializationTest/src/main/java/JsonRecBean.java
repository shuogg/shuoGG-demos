import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by shuoGG on 2018/9/28
 */
public class JsonRecBean implements Serializable {
    private int app;
    private long mac;
    private byte[] value;
    private String name;
    private List<Integer> list;
    private Map<String, Integer> map;

    public JsonRecBean() {
    }

    public int getApp() {
        return app;
    }

    public void setApp(int app) {
        this.app = app;
    }

    public long getMac() {
        return mac;
    }

    public void setMac(long mac) {
        this.mac = mac;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public Map<String, Integer> getMap() {
        return map;
    }

    public void setMap(Map<String, Integer> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "JsonRecBean{" +
                "app=" + app +
                ", mac=" + mac +
                ", value=" + Arrays.toString(value) +
                ", name='" + name + '\'' +
                ", list=" + list +
                ", map=" + map +
                '}';
    }
}
